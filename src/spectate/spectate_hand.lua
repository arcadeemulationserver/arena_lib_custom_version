minetest.register_node("arena_lib:spectate_hand", {
  description = "Spectate hand",
  wield_image = "arenalib_infobox_spectate.png",
  range = 0,
  groups = {not_in_creative_inventory = 1}
})
